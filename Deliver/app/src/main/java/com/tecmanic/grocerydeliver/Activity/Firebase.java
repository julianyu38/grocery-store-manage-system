package com.tecmanic.grocerydeliver.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.tecmanic.grocerydeliver.Config.SharedPrefManager;
import com.tecmanic.grocerydeliver.MainActivity;
import com.tecmanic.grocerydeliver.R;

public class Firebase extends AppCompatActivity {
    private Button buttonDisplayToken;
    private TextView textViewToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase);
        textViewToken = (TextView) findViewById(R.id.textViewToken);
        buttonDisplayToken = (Button) findViewById(R.id.buttonDisplayToken);

        buttonDisplayToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String token = SharedPrefManager.getInstance(Firebase.this).getDeviceToken();
                if (token != null) {
                    Log.d("DeviceToken", token);
                    textViewToken.setText(token);

                } else {
                    textViewToken.setText("Token not generated");
                }
            }
        });
        if (getIntent().getExtras() != null) {

            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                if (key.equals("MainActivity") && value.equals("True")) {
                }

            }
        }

        subscribeToPushService();

    }

    private void subscribeToPushService() {
        FirebaseMessaging.getInstance().subscribeToTopic("news");

        Log.d("AndroidBash", "Subscribed");
        Toast.makeText(Firebase.this, "Subscribed", Toast.LENGTH_SHORT).show();
        String token = FirebaseInstanceId.getInstance().getToken();
        // Log and toast
//        Log.d("Token", token);
        Toast.makeText(Firebase.this, token, Toast.LENGTH_SHORT).show();

    }
}
