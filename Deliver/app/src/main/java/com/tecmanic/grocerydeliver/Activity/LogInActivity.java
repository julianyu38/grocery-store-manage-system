package com.tecmanic.grocerydeliver.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tecmanic.grocerydeliver.AppController;
import com.tecmanic.grocerydeliver.Config.BaseURL;
import com.tecmanic.grocerydeliver.Config.SharedPref;
import com.tecmanic.grocerydeliver.Config.SharedPrefManager;
import com.tecmanic.grocerydeliver.MainActivity;
import com.tecmanic.grocerydeliver.R;
import com.tecmanic.grocerydeliver.util.ConnectivityReceiver;
import com.tecmanic.grocerydeliver.util.CustomVolleyJsonRequest;
import com.tecmanic.grocerydeliver.util.Session_management;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LogInActivity extends AppCompatActivity {
    EditText Et_login_email;
    RelativeLayout Btn_Sign_in;
    TextView tv_login_email;
    String getemail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_log_in);

//        String token1 = FirebaseInstanceId.getInstance().getToken();
//        String token = SharedPref.getString(LogInActivity.this,SharedPrefManager.getInstance(LogInActivity.this).getDeviceToken());
        Et_login_email = (EditText) findViewById(R.id.et_login_email);
        tv_login_email = (TextView) findViewById(R.id.tv_login_email);
        Btn_Sign_in = (RelativeLayout) findViewById(R.id.btn_Sign_in);

        getemail = Et_login_email.getText().toString();

        Btn_Sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Et_login_email.equals("")) {
                    Toast.makeText(LogInActivity.this, "Please Put Your Currect Email-Id", Toast.LENGTH_SHORT).show();
                } else {
                    makeLoginRequest();

                }
            }
        });


    }


    private void makeLoginRequest() {
        final String UserName = Et_login_email.getText().toString().trim();
        RequestQueue rq = Volley.newRequestQueue(this);
        StringRequest postReq = new StringRequest(Request.Method.POST, BaseURL.LOGIN,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("eclipse", "Response=" + response);
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            if (obj.getString("responce").equals("true")) {
                                JSONArray jsonArray = obj.getJSONArray("product");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    String user_id = jsonObject.getString("id");
                                    String user_fullname = jsonObject.getString("user_name");
                                    Session_management sessionManagement = new Session_management(LogInActivity.this);
                                    sessionManagement.createLoginSession(user_id, user_fullname);
                                    Intent intent = new Intent(LogInActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            } else {
                                Toast.makeText(LogInActivity.this, "Please Put Your Currect Email-Id", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error [" + error + "]");

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_password", UserName);
                return params;
            }
        };
        rq.add(postReq);

    }
}
